const Product = require('../models/products');
const fs = require('fs');


//Getting all the product !
exports.getAllProducts = (req, res, next) => {
    Product.find().then(
      (products) => {
        res.status(200).json(products);
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  };

//addition of a product 
exports.addProduct = (req, res, next) => {
    const thing = new Product({
      title: req.body.title,
      description: req.body.description,
      imageUrl: req.file.filename,
      price: req.body.price
    });
    thing.save()
      .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
      .catch(error => res.status(400).json({ error }));
  };


//getting one product by ID 
  exports.getOneProduct = (req, res, next) => {
    Product.findOne({
      _id: req.params.id
    }).then(
      (product) => {
        res.status(200).json(product);
      }
    ).catch(
      (error) => {
        res.status(404).json({
          error: error
        });
      }
    );
  };


//modify a product by its ID 
  exports.modifyProduct = (req, res, next) => {
    const productObject = req.file ?
      {
        title:req.body.title,
        description:req.body.description,
        price:req.body.price,
        imageUrl: req.file.filename
      } : { ...req.body };

      Product.findOne({ _id: req.params.id })
      .then(product => {
        const filename = product.imageUrl;
        console.log("aaaaaaaaaaaa", filename);
        fs.unlink(`images/${filename}`, () => {
            
          });
      })
      .catch(error => res.status(500).json({ error }));


    Product.updateOne({ _id: req.params.id }, { ...productObject, _id: req.params.id })
      .then(() => res.status(200).json({ message: 'Objet modifié !'}))
      .catch(error => res.status(400).json({ error }));
  };


  exports.deleteProduct = (req, res, next) => {
    Product.findOne({ _id: req.params.id })
      .then(product => {
        const filename = product.imageUrl;
        console.log("aaaaaaaaaaaa", filename);
        fs.unlink(`images/${filename}`, () => {
          Product.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
            .catch(error => res.status(400).json({ error }));
        });
      })
      .catch(error => res.status(500).json({ error }));
  };
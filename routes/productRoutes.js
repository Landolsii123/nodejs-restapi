const express = require('express');
const router = express.Router();

const multerConfig = require('../middleware/multerConfig');
const productCtrl=require('../controllers/productController');


router.get('/all',productCtrl.getAllProducts);
router.post('/add', multerConfig, productCtrl.addProduct);
router.get('/:id', productCtrl.getOneProduct);
router.put('/:id', multerConfig, productCtrl.modifyProduct);
router.delete('/:id', productCtrl.deleteProduct);



module.exports = router;